package com.example.definitiongame

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity() {
    private val wordList = mutableListOf<String>()
    private val definitionList = mutableListOf<String>()
    private val wordsToDefinitions = HashMap<String, String>()
    private val definitionsToDisplay = mutableListOf<String>()

    private val ADD_WORD_RESPONSE_CODE = 4242

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        readDefinitionFile()
        buildLists()
        chooseWordAndDefinitions()

        var myAdapter = ArrayAdapter<String>(
            this, android.R.layout.simple_list_item_1, definitionsToDisplay
        )
        defintionListView.adapter = myAdapter

        defintionListView.setOnItemClickListener { parent, view, position, id ->
            val word = wordText.text.toString()
            val correct = wordsToDefinitions[word]
            if (correct.equals(definitionsToDisplay[position])) {
                Toast.makeText(this, "Good job!", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "$word means \"$correct\"", Toast.LENGTH_LONG).show()
            }
            chooseWordAndDefinitions()
            myAdapter.notifyDataSetChanged()
        }
    }

    private fun chooseWordAndDefinitions() {
        val r = Random()
        wordText.text = wordList[r.nextInt(wordList.size)]

        definitionsToDisplay.clear()
        definitionList.shuffle() // Pick four random definitions.
        definitionsToDisplay.addAll(definitionList.subList(0, 4))
        definitionsToDisplay.add(wordsToDefinitions[wordText.text.toString()]!!)
        definitionsToDisplay.shuffle()
    }

    private fun readDefinitionFile() {
        val input = Scanner(getResources().openRawResource(R.raw.words))
        while (input.hasNextLine()) {
            val result = input.nextLine().split("-")
            val word = result[0].trim()
            val definition = result[1].trim()

            wordsToDefinitions[word] = definition
        }
    }

    private fun buildLists() {
        for ((word, definition) in wordsToDefinitions) {
            wordList.add(word)
            definitionList.add(definition)
        }
    }

    fun addWordClick(view: View) {
        val myIntent = Intent(this, AddWordActivity::class.java)
        // startActivity(myIntent) // We do not expect a "return value".
        startActivityForResult(myIntent, ADD_WORD_RESPONSE_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == ADD_WORD_RESPONSE_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val word = data.getStringExtra("word")
                val definition = data.getStringExtra("definition")

                wordsToDefinitions[word] = definition
                wordList.add(word)
                definitionList.add(definition)
            }
        }
    }


}

package com.example.definitiongame

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_add_word.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.wordText

class AddWordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_word)
    }

    fun addButtonClick(view: View) {
        val word = wordText.text.toString()
        val definition = definitionText.text.toString()

        val myIntent = Intent()
        myIntent.putExtra("word", word)
        myIntent.putExtra("definition", definition)
        setResult(Activity.RESULT_OK, myIntent)
        finish()
    }
}

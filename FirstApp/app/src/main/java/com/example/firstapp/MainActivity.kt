package com.example.firstapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private var score = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        pickRandomNumbers()
    }

    fun buttonClick(view: View) {
        val me: Int
        val other: Int

        if (view == leftButton) {
            me = leftButton.text.toString().toInt()
            other = rightButton.text.toString().toInt()
        } else {
            me = rightButton.text.toString().toInt()
            other = leftButton.text.toString().toInt()
        }

        if (me > other) {
            score++
            Toast.makeText(this, "You got it!", Toast.LENGTH_SHORT).show()
        } else {
            score--
            Toast.makeText(this, "Maybe SAU will take you.", Toast.LENGTH_SHORT).show()
        }

        scoreText.text = "Score: " + score
        pickRandomNumbers()
    }

//    fun rightButtonClick(view: View) {
////        Log.d("Whatever", "test")
//        val left = leftButton.text.toString().toInt()
//        val right = rightButton.text.toString().toInt()
//
//        if (right > left) {
//            score++
//        } else {
//            score--
//        }
//
//        scoreText.text = "Score: " + score
//        pickRandomNumbers()
//    }
//
//    fun leftButtonClick(view: View) {
////        Log.d("Whatever", "test")
//        val left = leftButton.text.toString().toInt()
//        val right = rightButton.text.toString().toInt()
//
//        if (left > right) {
//            score++
//        } else {
//            score--
//        }
//
//        scoreText.text = "Score: " + score
//        pickRandomNumbers()
//    }

    private fun pickRandomNumbers() {
        val left = Random.nextInt(1,10)
        var right: Int
        right = Random.nextInt(1,10)

        while (left == right) {
            right = Random.nextInt(1,10)
        }

        leftButton.text = left.toString()
        rightButton.text = right.toString()
    }
}

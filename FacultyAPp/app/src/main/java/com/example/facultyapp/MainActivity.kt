package com.example.facultyapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        nameListView.setOnItemClickListener { _, _, position, _ ->
            val selectedPicture = when (position) {
                0 -> R.drawable.js
                1 -> R.drawable.sh
                2 -> R.drawable.kd
                3 -> R.drawable.db
                else -> R.drawable.jm
            }
            picture.setImageResource(selectedPicture)
        }
    }

//    fun radioButtonClick(clickedItem: View) {
//        val selectedPicture = when (clickedItem) {
//            sykes ->  R.drawable.js
//            hennagin -> R.drawable.sh
//            dwelle -> R.drawable.kd
//            buscher -> R.drawable.db
//            else -> R.drawable.jm
//        }
//        picture.setImageResource(selectedPicture)
//    }
}

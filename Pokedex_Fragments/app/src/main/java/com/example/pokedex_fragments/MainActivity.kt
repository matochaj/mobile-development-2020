package com.example.pokedex_fragments

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun pokemonClick(view: View) {
        val tag = view.tag.toString()

        val myIntent = Intent(this, DetailsActivity::class.java)
        myIntent.putExtra("pokemonName", tag)
        startActivity(myIntent)
    }
}

package com.example.pokedex_fragments


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_details.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class DetailsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val act = activity
        if (act?.intent != null) {
            val pokemonName = act.intent.getStringExtra("pokemonName")
            pokemonNameText.text = pokemonName?.capitalize()

            // Access the files, based on the name as a String.
            val imageID = resources.getIdentifier(pokemonName, "drawable", activity!!.packageName)
            pokemonImage.setImageResource(imageID)

            val fileID = resources.getIdentifier(pokemonName, "raw", activity!!.packageName)
            val inputFile = Scanner(resources.openRawResource(fileID))

            descriptionText.text = inputFile.nextLine()
            inputFile.close()
        }
    }

    fun pokemonClick(view: View) {
        val tag = view.tag.toString()

        val myIntent = Intent(activity, DetailsActivity::class.java)
        myIntent.putExtra("pokemonName", tag)
        startActivity(myIntent)
    }

}

package com.example.pokedex_fragments


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import kotlinx.android.synthetic.main.fragment_pokedex.*

class PokedexFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pokedex, container, false)
    }

    private fun getAllViews(viewGroup: ViewGroup, viewList: MutableList<View>) {
        if (viewGroup.childCount > 0) {
            for (child in viewGroup.children) {
                if (child is ViewGroup) {
                    getAllViews(child, viewList)
                } else {
                    viewList.add(child)
                }
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val viewList = mutableListOf<View>()
        getAllViews(pokemon_table, viewList)
        Log.i("*****", "Total views: ${viewList.size}")


        for (view in viewList) {
            view.addOnClick
        }

        for (i in 0..pokemon_table.childCount-1) {
            val child = pokemon_table.getChildAt(i)
            if (child is ViewGroup)
                for (j in 0..child.childCount) {
                    Log.i("pokemon", child.getChildAt(i).toString())
                }
        }
    }

}
